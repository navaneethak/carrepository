# CarRepository
- Development env: vs2015
- Tools: Postman, resharper
- Possible Designs:
- Design-1:
- Svc layer -> BLL -> DAL
-	DI to access from one layer to another
-	BLL will hold the 24 hour calculation logic
- Design-2:
- Svc layer � BLL �> DAL -> DB 
-	DI to access from one layer to another
-	Sp in db will hold the 24 hr calc logic
- Current Design:
- Service layer -> Dal (Repository layer)
-	The service layer holds the business logic� Quick and dirty method. The repository stores the dummy data. No interfaces or DI used here
-	Data is hard-coded in the repository layer. So, no data insertion possible
- Assumptions/Constraints:
- 1.	Only a fixed set of cars can be booked for a fixed set of dates
- 2.	Only one car can be booked at a time
- Operation 1:
- GetAvailableCarList: Gets all the available cars. No request parameter.
- Response:
{
    "CarList": [
        {
            "CarNumber": "VIC321",
            "BookingDate": "2018-06-27",
            "IsBooked": false
        },
        {
            "CarNumber": "VIC322",
            "BookingDate": "2018-06-27",
            "IsBooked": false
        },
        {
            "CarNumber": "VIC323",
            "BookingDate": "2018-06-27",
            "IsBooked": false
        },
        {
            "CarNumber": "VIC324",
            "BookingDate": "2018-06-27",
            "IsBooked": false
        },
        {
            "CarNumber": "VIC325",
            "BookingDate": "2018-06-27",
            "IsBooked": false
        },
        {
            "CarNumber": "VIC326",
            "BookingDate": "2018-06-27",
            "IsBooked": false
        },
        {
            "CarNumber": "VIC327",
            "BookingDate": "2018-06-27",
            "IsBooked": false
        },
        {
            "CarNumber": "VIC328",
            "BookingDate": "2018-06-27",
            "IsBooked": false
        }
    ]
}
 
- Operation 2:
- UpdateCarBooking: Updates or books one car for a booking date available in the db.
- Request:
{
  "CarNumber":  "VIC321",
  "BookingDate": "2018-06-27"
}
- Response:
Message object
 
- Request no. 2:
{
  "CarNumber":  "cczc",
  "BookingDate": "2018-06-27"
}
- Response:
{
    "Message": "Car Number cczc is unavailable for the date 2018-06-27"
}
- setup:
- Create a vd in inetmgr with name such as carapis
- point physical path to subfolder containing .svc file
- Give a unique port name while creating the vd
- This port name is used while invoking the api using postman (in above scr shots)
- Ensure corresponding App pool points to v4 of .net






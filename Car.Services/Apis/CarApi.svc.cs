﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Car.Repositories;
using CarApi.Helpers;
using CarApi.Interfaces;
using CarApi.Models;

namespace CarApi
{
    public class CarApi1 : ICarApi
    {

        public SuccessResponse UpdateCarBooking(Models.Car car)

        {

            var success = new SuccessResponse();

            try
            {
                var carRepository = new CarRepository();
                var carlistfromrepo = carRepository.GetCars();
                var carnum = car.CarNumber.Trim();
                var bookingdate = car.BookingDate.Trim();
                var isValidCar = carlistfromrepo.Any(carvar => carvar.CarNumber == carnum);
                var isCarAvailableOnDate = carlistfromrepo.Any(carvar => carvar.CarNumber == carnum && carvar.BookingDate == bookingdate);
                if (!isCarAvailableOnDate)
                {
                    success.Message = "Car Number " + carnum + " is unavailable for the date " + bookingdate;
                    return success;
                }
                if (!isValidCar)
                {
                    success.Message = "Car Number " + carnum + " is unavailable in the database";
                    return success;
                }
                var dtBookDateTime = DateTime.ParseExact(car.BookingDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                if (dtBookDateTime < DateTime.Now)
                {
                    success.Message = "Booking Date cannot be less than Current Date";
                    return success;
                }
                
                foreach (var carvar in from carvar in carlistfromrepo
                    where carvar.IsBooked
                    let dateTimeSpandiff = DateTime.Now - Convert.ToDateTime(carvar.BookingDate)
                    let hours = dateTimeSpandiff.TotalHours
                    where hours > 24
                    select carvar)
                    carvar.IsBooked = false;
                
                var carfinallist = from carlist in carlistfromrepo
                    where carlist.IsBooked == false
                    select carlist;

                foreach (var carbooking in carfinallist.Where(carbooking => carbooking.CarNumber.Trim().Equals(car.CarNumber.Trim(), StringComparison.OrdinalIgnoreCase)).Where(carbooking => carbooking.BookingDate.Trim() == car.BookingDate.Trim()))
                    if (!carbooking.IsBooked)
                    {
                        carbooking.IsBooked = true;
                        success.Message = "Car number " + car.CarNumber + " is successfully booked for you on " +
                                          car.BookingDate;
                    }
                    else
                        success.Message = "Car number " + car.CarNumber + "is already booked on " +
                                          car.BookingDate + ". Choose another car";
            }
            catch (Exception ex)
            {
                throw new ApiException(ex, HttpStatusCode.InternalServerError, logMsg: "Error in UpdateCarBooking method: " + ex.Message);
            }

            return success;

        }

        public CarListing GetAvailableCarList()
        {
            var carlisting = new CarListing();
            try
            {
                var carRepository=new CarRepository();
                var carlistfromrepo = carRepository.GetCars();
                var cars= carlistfromrepo.Select(repcar => new Models.Car
                                                           {
                                                               CarNumber = repcar.CarNumber, BookingDate = repcar.BookingDate, IsBooked = repcar.IsBooked
                                                           }).ToList();
                IEnumerable<Models.Car> carlist = from car in cars
                                              where car.IsBooked == false
                                                     select car;
                carlisting.CarList = carlist;
            }
            catch (Exception ex)
            {
                throw new ApiException(ex, HttpStatusCode.InternalServerError, logMsg: "Error in GetAvailableCarList method: " + ex.Message);
            }
            return carlisting;
        }
    }
}

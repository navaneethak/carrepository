﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel.Web;
using CarApi.Models;
using log4net;

namespace CarApi.Helpers
{
    [Serializable]
    public class ApiException : Exception
    {
        private ILog Logger
        {
            get { return LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType); }
        }
        private const string UnauthorisedUserErrorMsg = "Unauthorised user";
        private const string ResourceNotFoundErrorMsg = "Resource not found";
        private const string BadRequestFoundErrorMsg = "Bad request";
        private const string InternalServerErrorMsg = "The server encountered an issue whilst trying to fulfill the request";
        private const string UnprocessableEntityMsg = "Invalid request parameter";

        public ApiException(string errorMessage)
        {
            Logger.Error(errorMessage);

            var error = new ErrorFaultException
            {
                ErrorCode = HttpStatusCode.InternalServerError,
                Error = errorMessage
            };
            throw new WebFaultException<ErrorFaultException>(error, HttpStatusCode.Unused);
        }

        /// <summary>
        /// Catches the errors thrown from the API that has a valid status code
        /// </summary>
        /// <param name="thrownException"></param>
        /// <param name="statusCode"></param>
        /// <param name="errorMessage">if null, the default exception message associated with the API status code will be thrown </param>
        /// <param name="logMsg">if null, the error message will be logged</param>
        /// <param name="messageList">Pass the suberrors, or list of associated errors</param>
        public ApiException(Exception thrownException, HttpStatusCode statusCode, string errorMessage = null, string logMsg = null, List<SubError> messageList = null)
        {
            errorMessage = GetErrorMsg(statusCode, errorMessage);
            logMsg =  GetLogMsg(logMsg, errorMessage, messageList);

            Logger.Error(logMsg, thrownException);

            var error = new ErrorFaultException
            {
                ErrorCode = statusCode,
                Error = errorMessage,
                Messages = messageList
            };
            throw new WebFaultException<ErrorFaultException>(error, statusCode);
        }

       
        private string GetErrorMsg(HttpStatusCode statusCode, string customErrormsg)
        {
            if (!string.IsNullOrEmpty(customErrormsg))
                return customErrormsg;

            switch (statusCode)
            {
                case HttpStatusCode.Unauthorized:
                    return UnauthorisedUserErrorMsg;
                    break;
                case HttpStatusCode.NotFound:
                    return ResourceNotFoundErrorMsg;
                    break;
                case HttpStatusCode.BadRequest:
                    return BadRequestFoundErrorMsg;
                    break;
                case HttpStatusCode.InternalServerError:
                    return InternalServerErrorMsg;
                    break;
                case (HttpStatusCode)422:
                    return UnprocessableEntityMsg;
                    break;
                default:
                    return InternalServerErrorMsg;
            }
        }

        private string GetLogMsg(string logMsg, string errorMesage, List<SubError> messageList)
        {
            var sb = new System.Text.StringBuilder();
            
            if(messageList != null)
            {
                sb.AppendLine("");
                foreach(var m in messageList)
                {
                    sb.AppendLine(string.Format("\tSub error code: {0}, Sub message: {1}", m.ErrorCode, m.ErrorMessage));
                }
            }

            return string.Format("{2} Error: {0}" +
                                 "\n{1}", errorMesage, sb, logMsg);
        }


       
    }
}
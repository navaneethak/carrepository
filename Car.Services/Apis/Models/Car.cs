﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CarApi.Models
{
    [DataContract]
    public class Car
    {
       
        [DataMember(Name = "CarNumber", Order = 0)]
        public string CarNumber { get; set; }

        [DataMember(Name = "BookingDate", Order = 1)]
        public string BookingDate { get; set; }

        [DataMember(Name = "IsBooked", Order = 2)]
        public bool IsBooked { get; set; }
    }

    [DataContract]
    public class CarListing
    {

        [DataMember(Name = "CarList", Order = 0)]
        public IEnumerable<Car> CarList { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization;

namespace CarApi.Models
{
    [DataContract]
    public class ErrorFaultException
    {
        [DataMember(EmitDefaultValue = false, Order = 0)]
        public HttpStatusCode ErrorCode { get; set; }

        [DataMember(EmitDefaultValue = false, Order = 1)]
        public string Error { get; set; }

        [DataMember(EmitDefaultValue = false, Order = 2)]
        public List<SubError> Messages { get; set; }
    }

    [DataContract]
    public class SubError
    {
        [DataMember(EmitDefaultValue = false, Order = 0)]
        public string ErrorCode { get; set; }

        [DataMember(EmitDefaultValue = false, Order = 1)]
        public string ErrorMessage { get; set; }
    }
}
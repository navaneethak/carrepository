﻿using System.Runtime.Serialization;

namespace CarApi.Models
{
    [DataContract]
    public class SuccessResponse
    {
        [DataMember]
        public string Message { get; set; }
    }
}
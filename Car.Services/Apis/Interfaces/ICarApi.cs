﻿using System.ServiceModel;
using System.ServiceModel.Web;
using CarApi.Models;

namespace CarApi.Interfaces
{
    [ServiceContract]
    public interface ICarApi
    {

        [OperationContract]
        [FaultContract(typeof(ErrorFaultException))]
        [WebInvoke(Method = "POST",
        UriTemplate = "updatebooking",
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json)]
        SuccessResponse UpdateCarBooking(Models.Car car);

        [OperationContract]
        [FaultContract(typeof(ErrorFaultException))]
        [WebGet(ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "availablecars")]
        CarListing GetAvailableCarList();
    }
    
}

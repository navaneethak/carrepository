﻿namespace Car.Repositories.Models
{
    public class Car
    {
        public string CarNumber { get; set; }
        public string BookingDate { get; set; }
        public bool IsBooked { get; set; }
    }
}

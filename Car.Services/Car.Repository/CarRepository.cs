﻿using System.Collections.Generic;

namespace Car.Repositories
{
    public class CarRepository
    {
        //dummy repository, ideally must pick from db...
        public List<Models.Car> GetCars()
        {
            var cars=new List<Models.Car>();
            var car1 = new Models.Car
                      {
                          CarNumber = "VIC321",
                          BookingDate = "2018-06-30",
                          IsBooked = false
                      };
            cars.Add(car1);
            var car2 = new Models.Car
            {
                CarNumber = "VIC322",
                BookingDate = "2018-06-29",
                IsBooked = false
            };
            cars.Add(car2);
            var car3 = new Models.Car
            {
                CarNumber = "VIC323",
                BookingDate = "2018-07-27",
                IsBooked = false
            };
            cars.Add(car3);
            var car4 = new Models.Car
            {
                CarNumber = "VIC324",
                BookingDate = "2018-07-31",
                IsBooked = false
            };
            cars.Add(car4);
            var car5 = new Models.Car
            {
                CarNumber = "VIC325",
                BookingDate = "2018-07-27",
                IsBooked = false
            };
            cars.Add(car5);
            var car6 = new Models.Car
            {
                CarNumber = "VIC326",
                BookingDate = "2018-07-07",
                IsBooked = false
            };
            cars.Add(car6);
            var car7 = new Models.Car
            {
                CarNumber = "VIC327",
                BookingDate = "2018-08-27",
                IsBooked = false
            };
            cars.Add(car7);
            var car8 = new Models.Car
            {
                CarNumber = "VIC328",
                BookingDate = "2018-09-27",
                IsBooked = false
            };
            cars.Add(car8);
            var car9 = new Models.Car
            {
                CarNumber = "VIC329",
                BookingDate = "2018-06-27",
                IsBooked = true
            };
            cars.Add(car9);
            var car10 = new Models.Car
            {
                CarNumber = "VIC330",
                BookingDate = "2018-06-26",
                IsBooked = true
            };
            cars.Add(car10);
            return cars;
        }
        
    }
}
